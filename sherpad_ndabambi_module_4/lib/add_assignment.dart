import 'package:flutter/material.dart';
import 'package:sherpad_ndabambi_module_4/assignment.dart';
import 'package:sherpad_ndabambi_module_4/colors.dart';
import 'package:sherpad_ndabambi_module_4/dashboard.dart';

class AddAssignment extends StatelessWidget {
  AddAssignment({super.key, required this.title});

  final String title;
  final nameController = TextEditingController();
  final subjectController = TextEditingController();
  final dueDateController = TextEditingController();
  final dueTimeController = TextEditingController();
  final scopeController = TextEditingController();

  void dispose() {
    nameController.dispose();
    subjectController.dispose();
    dueDateController.dispose();
    dueTimeController.dispose();
    scopeController.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(title),
      ),
      body: Column(
        children: [
          const Padding(
            padding: EdgeInsets.symmetric(vertical: 16),
            child: Image(
              image: AssetImage('assets/todo-svgrepo-com.png'),
              width: 64,
            ),
          ),
          const Text(
            "Add an assignment",
            textScaleFactor: 2,
          ),
          Padding(
            padding: const EdgeInsets.fromLTRB(64, 16, 64, 0),
            child: Column(
              children: [
                TextFormField(
                  controller: nameController,
                  decoration: InputDecoration(
                    labelText: 'Assignment name',
                    enabledBorder: UnderlineInputBorder(
                      borderSide: BorderSide(color: primary.shade400),
                    ),
                    focusedBorder: UnderlineInputBorder(
                      borderSide: BorderSide(color: primary),
                    ),
                  ),
                ),
                const SizedBox(height: 16),
                TextFormField(
                  controller: subjectController,
                  decoration: InputDecoration(
                    labelText: 'Subject',
                    enabledBorder: UnderlineInputBorder(
                      borderSide: BorderSide(color: primary.shade400),
                    ),
                    focusedBorder: UnderlineInputBorder(
                      borderSide: BorderSide(color: primary),
                    ),
                  ),
                ),
                const SizedBox(height: 16),
                TextFormField(
                  controller: dueDateController,
                  decoration: InputDecoration(
                    labelText: 'Due date (YYYY-MM-DD)',
                    enabledBorder: UnderlineInputBorder(
                      borderSide: BorderSide(color: primary.shade400),
                    ),
                    focusedBorder: UnderlineInputBorder(
                      borderSide: BorderSide(
                        color: primary,
                      ),
                    ),
                  ),
                ),
                const SizedBox(height: 16),
                TextFormField(
                  controller: dueTimeController,
                  decoration: InputDecoration(
                    labelText: 'Due time (HH:MM)',
                    enabledBorder: UnderlineInputBorder(
                      borderSide: BorderSide(color: primary.shade400),
                    ),
                    focusedBorder: UnderlineInputBorder(
                      borderSide: BorderSide(
                        color: primary,
                      ),
                    ),
                  ),
                ),
                const SizedBox(height: 16),
                TextFormField(
                  controller: scopeController,
                  decoration: InputDecoration(
                    labelText: 'Scope',
                    enabledBorder: UnderlineInputBorder(
                      borderSide: BorderSide(color: primary.shade400),
                    ),
                    focusedBorder: UnderlineInputBorder(
                      borderSide: BorderSide(
                        color: primary,
                      ),
                    ),
                  ),
                ),
                const SizedBox(height: 48),
                ElevatedButton(
                  onPressed: () {
                    assignments.add(
                      Assignment(
                        name: nameController.text,
                        subject: subjectController.text,
                        dueDate: DateTime.parse(
                            '${dueDateController.text} ${dueTimeController.text}'),
                        scope: scopeController.text,
                      ),
                    );
                    Navigator.push(
                      context,
                      MaterialPageRoute(
                          builder: (context) => Dashboard(
                                title: title,
                              )),
                    );
                  },
                  style: ElevatedButton.styleFrom(
                    padding: const EdgeInsets.symmetric(
                        horizontal: 32, vertical: 24),
                    shape: RoundedRectangleBorder(
                      borderRadius: BorderRadius.circular(50),
                    ),
                  ),
                  child: const Text('Done'),
                ),
              ],
            ),
          ),
        ],
      ),
    );
  }
}
