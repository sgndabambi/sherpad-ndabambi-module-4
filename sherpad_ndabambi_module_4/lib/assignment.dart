class Assignment {
  String name;
  String subject;
  DateTime dueDate;
  String scope;

  Assignment(
      {required this.name,
      required this.subject,
      required this.dueDate,
      required this.scope});
}

List<Assignment> assignments = <Assignment>[
  Assignment(
    name: "Assessment 3",
    subject: "MAT1503: Linear Algebra I",
    dueDate: DateTime.parse("2022-06-27 23:00"),
    scope: "Chapter 1 & 2 of HC"
        "\nUnit 1 & 2 of the Study Guide",
  ),
  Assignment(
    name: "Assessment 2",
    subject: "INF1505: Introduction to Business Information Systems",
    dueDate: DateTime.parse("2022-06-28 21:00"),
    scope: "Principles of Information Systems Chapters 4 to 7"
        "\nTutorial letter 102 units 5 to 7",
  ),
];
