import 'package:intl/intl.dart';
import 'package:flutter/material.dart';
import 'package:sherpad_ndabambi_module_4/add_assignment.dart';
import 'package:sherpad_ndabambi_module_4/assignment.dart';
import 'package:sherpad_ndabambi_module_4/colors.dart';
import 'package:sherpad_ndabambi_module_4/edit_profile.dart';

class Dashboard extends StatelessWidget {
  const Dashboard({super.key, required this.title});

  final String title;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(title),
      ),
      body: ListView.separated(
        padding: const EdgeInsets.all(8),
        itemCount: assignments.length,
        itemBuilder: (BuildContext context, int index) {
          return ListTile(
            shape: RoundedRectangleBorder(
              borderRadius: BorderRadius.circular(5),
            ),
            textColor: secondary,
            tileColor: primary,
            title: Padding(
              padding: const EdgeInsets.symmetric(vertical: 10, horizontal: 0),
              child: ListTile(
                shape: RoundedRectangleBorder(
                  borderRadius: BorderRadius.circular(4),
                ),
                tileColor: secondary,
                title: Text(assignments[index].name),
                subtitle: Text(assignments[index].subject),
              ),
            ),
            subtitle: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Row(
                  children: [
                    const Text("Due:\t\t\t"),
                    Text(DateFormat.yMMMd()
                        .add_Hm()
                        .format(assignments[index].dueDate)),
                  ],
                ),
                Row(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    const Text("Scope:\t"),
                    Text(assignments[index].scope),
                  ],
                ),
                Center(
                  child: Padding(
                    padding: const EdgeInsets.symmetric(vertical: 8),
                    child: ElevatedButton(
                      onPressed: () {
                        assignments.removeAt(index);
                        Navigator.push(
                          context,
                          MaterialPageRoute(
                              builder: (context) => Dashboard(
                                    title: title,
                                  )),
                        );
                      },
                      style: ElevatedButton.styleFrom(
                          fixedSize: const Size(110, 0),
                          primary: Colors.green,
                          shape: RoundedRectangleBorder(
                            borderRadius: BorderRadius.circular(50),
                          ),
                          textStyle: const TextStyle(
                            fontSize: 10,
                          )),
                      child: Row(
                        children: const [
                          Icon(
                            Icons.check,
                            size: 14,
                          ),
                          Text("Mark as done"),
                        ],
                      ),
                    ),
                  ),
                ),
              ],
            ),
          );
        },
        separatorBuilder: (BuildContext context, int index) => const Divider(),
      ),
      floatingActionButton: Padding(
        padding: const EdgeInsets.only(top: 80),
        child: Column(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: [
            FloatingActionButton(
              onPressed: () {
                Navigator.push(
                  context,
                  MaterialPageRoute(
                      builder: (context) => EditProfile(
                            title: title,
                          )),
                );
              },
              backgroundColor: Colors.grey,
              child: const Icon(Icons.account_circle),
            ),
            FloatingActionButton(
              onPressed: () {
                Navigator.push(
                    context,
                    MaterialPageRoute(
                        builder: (context) => AddAssignment(
                              title: title,
                            )));
              },
              child: const Icon(Icons.add),
            )
          ],
        ),
      ),
    );
  }
}
